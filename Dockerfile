FROM rasa/rasa:latest-full
USER root
ENV BOT_ENV=production
COPY . /var/www
WORKDIR /var/www
RUN python3 -m venv env
RUN source ./env/bin/activate 
RUN pip install --upgrade pip 
RUN pip install rasa==2.7.1
RUN python -m spacy download es_core_news_md
RUN python -m spacy link es_core_news_md es
RUN rasa train
ENTRYPOINT [ "rasa", "run", "-p", "5005","--enable-api","--cors","*","--debug"]